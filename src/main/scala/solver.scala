package pspz3

import com.microsoft.z3, z3._

// public ParamDescrs getParameterDescriptions()
// public void assertAndTrack(BoolExpr[] constraints, BoolExpr[] ps)
// public void assertAndTrack(BoolExpr constraint, BoolExpr p)

final class Solver(val solver: z3.Solver) extends ToS {
  import solver._

  def setParameters(p: z3.Params): this.type =
    try this finally solver setParameters p

  def satisfy(ps: BoolExpr*): SatisfyResult = {
    solver.add(ps: _*)
    solver.check() match {
      case Status.SATISFIABLE   => Satisfied(model)
      case Status.UNSATISFIABLE => Unsatisfiable(proof)
      case Status.UNKNOWN       => UnknownResult(reason)
    }
  }
  def backtracking[A](body: => A): A = {
    push()
    try body finally pop()
  }

  def assertions: Vector[BoolExpr]  = getAssertions.toVector
  def help: String                  = getHelp()
  def model: Model                  = new Model(getModel())
  def proof: Option[Expr]           = try Option(getProof()) catch { case _: Z3Exception => None }
  def reason: String                = getReasonUnknown
  def statistics: Statistics        = getStatistics
  def to_s                          = assertions mkString ", "
  def unsatisfied: Vector[BoolExpr] = getUnsatCore.toVector
}


/***

final case class Bounded[A](lo: A, value: A, hi: A) extends ToS {
  def map[B](f: A => B): Bounded[B] = Bounded(f(lo), f(value), f(hi))
  def to_s = s"$lo <= $value <= $hi"
}
class Z3Handle(handle: z3.Optimize#Handle) extends ToS {
  def lower: ArithExpr              = handle.getLower
  def value: ArithExpr              = handle.getValue
  def upper: ArithExpr              = handle.getUpper
  def toBounded: Bounded[ArithExpr] = Bounded(lower, value, upper)

  def to_s  = s"$lower <= $value <= $upper"
}
class Z3Optimize(opt: Optimize) extends ToS {
  def check(): Status                                 = opt.Check()
  def constrain(constraints: BoolExpr*): this.type    = try this finally opt.Assert(constraints: _*)
  def help: String                                    = opt.getHelp()
  def maximizing(expr: ArithExpr): z3.Optimize#Handle = opt.MkMaximize(expr)
  def minimizing(expr: ArithExpr): z3.Optimize#Handle = opt.MkMinimize(expr)
  def model: Model                                    = Model(opt.getModel())
  def statistics: Statistics                          = opt.getStatistics()
  def to_s: String                                    = "" + statistics
  def withParams(p: Params): this.type                = try this finally opt.setParameters(p)
}
***/
