package pspz3

import com.microsoft.z3._

sealed trait Z3Result
sealed trait SatisfyResult extends Z3Result
sealed trait ProofResult extends Z3Result

final case class UnknownResult(reason: String)      extends SatisfyResult with ProofResult
final case class Satisfied(model: Model)            extends SatisfyResult
final case class Unsatisfiable(proof: Option[Expr]) extends SatisfyResult
final case class Proven(result: Unsatisfiable)      extends ProofResult
final case class Disproven(result: Satisfied)       extends ProofResult

object ProofResult {
  implicit class ProofResultOps(result: ProofResult) {
    def isProven = result match {
      case Proven(_) => true
      case _         => false
    }
    def isDisproven = result match {
      case Disproven(_) => true
      case _            => false
    }
  }
}

object SatisfyResult {
  implicit class SatisfyResultOps(result: SatisfyResult) extends ToS {
    def isSatisfied = result match {
      case Satisfied(_) => true
      case _            => false
    }
    def isUnsatisfiable = result match {
      case Unsatisfiable(_) => true
      case _                => false
    }
    def to_s: String = result match {
      case Satisfied(Model(m))       => s"satisfied by $m"
      case Satisfied(_)              => "satisfied, no model available"
      case Unsatisfiable(Some(expr)) => s"unsatisfiable, proof: $expr"
      case Unsatisfiable(None)       => "unsatisfiable, no proof available"
      case UnknownResult(msg)        => "unknown: $msg"
    }
  }
}
