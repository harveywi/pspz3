package pspz3

final class AnyOps[A](val __x: A) extends AnyVal {
  @inline def as[B] : B             = __x.asInstanceOf[B]
  @inline def |>[B](f: A => B): B   = f(__x)
  @inline def doto(f: A => Unit): A = { f(__x) ; __x }
}

final class ClassLoaderOps(val cl: ClassLoader) extends AnyVal {
  def resourceNames() = Resources.getResourceNames(cl, jPath(""))
}
