package pspz3

import com.microsoft.z3

/** An interpretation of a function or expression within the model.
 *  Nullary functions are interpreted as expressions, higher arities
 *  receive a FuncInterp.
 */
sealed trait Interpretation
object Interpretation {
  final case class Expression(expr: Expr) extends Interpretation
  final case class Function(fi: z3.FuncInterp) extends Interpretation
  final case object None extends Interpretation

  def apply(e: Expr): Interpretation           = if (e == null) None else Expression(e)
  def apply(fi: z3.FuncInterp): Interpretation = if (fi == null) None else Function(fi)
}

// A Model contains interpretations (assignments) of constants and functions.
final class Model(val model: z3.Model) extends AnyVal with ToS {
  import model._
  type Funcs = Array[z3.FuncDecl]

  def interpret(a: Expr): Interpretation = Interpretation(model getConstInterp a)
  def interpret(f: z3.FuncDecl): Interpretation = f.getArity match {
    case 0 => Interpretation(model getConstInterp f)
    case _ => Interpretation(model getFuncInterp f)
  }

  def assigned(): Vector[Assign[Expr]]  = consts.toVector map (f => Assign(f, eval(f))) sortBy (_.name)
  def consts(): Funcs                   = getConstDecls()
  def funcs(): Funcs                    = getFuncDecls()
  def decls(): Funcs                    = getDecls()
  def sorts(): Array[z3.Sort]           = getSorts()
  def evalName(name: String): Opt[Expr] = consts find (_.getName.toString == name) map (x => eval(x))
  def eval(f: z3.FuncDecl): Expr        = eval(f())
  def eval(t: Expr): Expr               = evaluate(t, false) // complete = false
  def complete(t: Expr): Expr           = evaluate(t, true)
  def universe(s: z3.Sort): Array[Expr] = getSortUniverse(s)

  def to_s = s"$getNumConsts consts, $getNumFuncs funcs, $getNumSorts sorts"
  def isEmpty = model eq null
}

object Model {
  val None                                    = new Model(null)
  def apply(model: z3.Model): Model           = if (model == null) None else new Model(model)
  def unapply(model: Model): Option[z3.Model] = Option(model.model)
}
