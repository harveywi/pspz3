import com.microsoft.z3
import z3.enumerations.Z3_lbool
import scala.collection.{ mutable, immutable }

/** Z3 notes
 *
 *  Z3Object >: { ApplyResult AST ASTMap ASTVector Constructor ConstructorList Fixedpoint FuncInterp } ++
 *              { Entry Goal Model Optimize ParamDescrs Params Probe Solver Statistics Symbol Tactic }
 *
 *  AST >: { Expr, FuncDecl, Pattern, Sort }
 */
package object pspz3 {
  type ArithExpr  = z3.ArithExpr
  type BitVecExpr = z3.BitVecExpr
  type BoolExpr   = z3.BoolExpr
  type Context    = z3.Context
  type Expr       = z3.Expr
  type FPExpr     = z3.FPExpr
  type IntExpr    = z3.IntExpr
  type RealExpr   = z3.RealExpr

  type SignedBV   = BitVecExpr @@ Signed
  type UnsignedBV = BitVecExpr @@ Unsigned
  type Int32      = SignedBV
  type UInt32     = UnsignedBV
  type Int64      = SignedBV
  type UInt64     = UnsignedBV

  type Opt[+A]         = scala.Option[A]
  type BigInteger      = java.math.BigInteger
  type CTag[A]         = scala.reflect.ClassTag[A]
  type jEnumeration[A] = java.util.Enumeration[A]
  type jFile           = java.io.File
  type jHashMap[K, V]  = java.util.HashMap[K, V]
  type jJar            = java.util.jar.JarFile
  type jMap[K, V]      = java.util.Map[K, V]
  type jPath           = java.nio.file.Path
  type jStringMap      = jMap[String, String]
  type jUrl            = java.net.URL

  type ExprSort = pspz3.context.ExprSort

  type Tagged[U] = Any { type Tag = U }
  type @@[T, U] = T with Tagged[U]

  implicit def anyOps[A](x: A): AnyOps[A]                        = new AnyOps[A](x)
  implicit def classLoaderOps[A](x: ClassLoader): ClassLoaderOps = new ClassLoaderOps(x)

  def abort(msg: String): Nothing                 = throw new RuntimeException(msg)
  def ctag[A](implicit z: CTag[A]): CTag[A]       = z
  def echo(lines: Any*): Unit                     = lines foreach (Console println _)
  def jMap[K, V](kvs: (K, V)*): jMap[K, V]        = new jHashMap[K, V] doto (xs => for ((k, v) <- kvs) xs.put(k, v))
  def jPath(s: String): jPath                     = java.nio.file.Paths get s
  def mutableMap[K, V](kvs: (K, V)*)              = mutable.Map(kvs: _*)
  def println[A](x: A)(implicit z: Show[A]): Unit = printlns(z shown x)
  def printlns(xs: ToS*): Unit                    = xs foreach (Console println _)
}
