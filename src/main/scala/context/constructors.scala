package pspz3
package context

import com.microsoft.z3

trait Constructors extends Any with HasContext {
  // Constructor mkConstructor(String name, String recognizer, String[] fieldNames, Sort[] sorts, int[] sortRefs)
  // Fixedpoint mkFixedpoint()
  // Goal mkGoal(boolean models, boolean unsatCores, boolean proofs)
  // IntSymbol mkSymbol(int i)
  // Optimize mkOptimize()
  // Params mkParams()
  // Pattern mkPattern(Expr... terms)
  // Solver mkSolver(String logic)
  // StringSymbol mkSymbol(String name)
  // Solver mkSimpleSolver()
  // Solver mkSolver()

  def solver: Solver = new Solver(ctx.mkSolver())
}
