package pspz3
package context

import com.microsoft.z3

final case class BigRational(n: BigInt, d: BigInt) extends ToS {
  def toBigDecimal: BigDecimal = BigDecimal(n) / BigDecimal(d)
  def toDouble: Double         = toBigDecimal.toDouble

  def to_s = s"$n/$d"
}
trait Exprs extends Any with HasContext {
  self: Bools =>

  implicit class ExprOps[E <: Expr](lhs: E) {
    def ===(rhs: E): BoolExpr = ctx.mkEq(lhs, rhs)
    def !==(rhs: E): BoolExpr = !(this === rhs)

    def toIntExpr: Opt[IntExpr] = lhs match {
      case x: z3.IntExpr => Some(x)
      case _             => None
    }
    def toIntNum: Opt[z3.IntNum] = lhs match {
      case x: z3.IntNum => Some(x)
      case _            => None
    }
    def toRealExpr: Opt[z3.RealExpr] = lhs match {
      case x: z3.RealExpr => Some(x)
      case _              => None
    }
    def toRatNum: Opt[z3.RatNum] = lhs match {
      case x: z3.RatNum => Some(x)
      case _            => None
    }

    /** Extract value as the given type if it's known. */
    def toInt32: Opt[Int]     = toIntNum map (_.getInt)
    def toInt64: Opt[Long]    = toIntNum map (_.getInt64)
    def toBigInt: Opt[BigInt] = toIntNum map (n => BigInt(n.getBigInteger))

    def toBigRational: Opt[BigRational] = for {
      x <- toRatNum
      n <- x.getNumerator.toBigInt
      d <- x.getDenominator.toBigInt
    } yield BigRational(n, d)
  }
}
