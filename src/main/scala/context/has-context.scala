package pspz3
package context

import com.microsoft.z3, z3._

final case class Z3Param(name: Symbol, sort: Sort)
final case class Z3Func(params: Seq[Z3Param], body: Expr)
final case class Z3Patterns(patterns: Seq[Pattern], noPatterns: Seq[Expr])
final case class Z3SkolemizedId(quantifierId: Symbol, skolemId: Symbol)
final case class Z3QuantifierInfo(id: Z3SkolemizedId, isForall: Boolean, weight: Int)

trait HasContext extends Any {
  val ctx: Context

  def quantifier(info: Z3QuantifierInfo, fn: Z3Func, pats: Z3Patterns) = ctx.mkQuantifier(
    info.isForall,
    fn.params.map(_.sort).toArray,
    fn.params.map(_.name).toArray,
    fn.body,
    info.weight,
    pats.patterns.toArray,
    pats.noPatterns.toArray,
    info.id.quantifierId,
    info.id.skolemId
  )
}

trait ContextSyntax extends AnyRef
     with Arrays
     with BitVecs
     with Bools
     with Constructors
     with Exprs
     with Funcs
     with Ints
     with Probes
     with Reals
     with SmtLib
     with Sorts
     with Tactics { }

final class Z3Context(val ctx: Context) extends ContextSyntax {
  def sat[E1](f: E1 => BoolExpr)(implicit z1: ExprSort { type Expr = E1 }): Opt[z1.Value] = {
    solver satisfy f(z1 const "x") match {
      case Satisfied(model) => model evalName "x" flatMap (z1 eval _.as[E1])
      case _                => None
    }
  }
  def sat[E1, E2](f: (E1, E2) => BoolExpr)(implicit
    z1: ExprSort { type Expr = E1 },
    z2: ExprSort { type Expr = E2 }
  ): Opt[(z1.Value, z2.Value)] = {

    solver satisfy f(z1 const "x", z2 const "y") match {
      case Satisfied(model) =>
        val r1 = model evalName "x" flatMap (z1 eval _.as[E1])
        val r2 = model evalName "y" flatMap (z2 eval _.as[E2])
        (r1, r2) match {
          case (Some(r1), Some(r2)) => Some(r1 -> r2)
          case _                    => None
        }
      case _                => None
    }
  }
}

object Z3Context {
  def apply(): Z3Context = new Z3Context(new Context())
}
