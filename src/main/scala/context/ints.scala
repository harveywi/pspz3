package pspz3
package context

import com.microsoft.z3

trait Ints extends Any with HasContext {
  self: Bools with Exprs with Sorts =>

  import ctx._

  implicit def liftLong(n: Long): IntExpr     = INT literal n
  implicit def liftBigInt(n: BigInt): IntExpr = INT literal n

  implicit class ArithExprOps(lhs: ArithExpr) {
    def unary_- : ArithExpr = mkUnaryMinus(lhs)

    def <(rhs: ArithExpr): BoolExpr   = mkLt(lhs, rhs)
    def <=(rhs: ArithExpr): BoolExpr  = mkLe(lhs, rhs)
    def >(rhs: ArithExpr): BoolExpr   = mkGt(lhs, rhs)
    def >=(rhs: ArithExpr): BoolExpr  = mkGe(lhs, rhs)

    def +(rhs: ArithExpr): ArithExpr  = mkAdd(lhs, rhs)
    def *(rhs: ArithExpr): ArithExpr  = mkMul(lhs, rhs)
    def /(rhs: ArithExpr): ArithExpr  = mkDiv(lhs, rhs)
    def -(rhs: ArithExpr): ArithExpr  = mkSub(lhs, rhs)
    def **(rhs: ArithExpr): ArithExpr = mkPower(lhs, rhs)

    def abs: ArithExpr = (lhs >= 0).cond(lhs, -lhs)
  }

  implicit class IntExprOps(lhs: IntExpr) {
    def toInt32: SignedBV = mkInt2BV(32, lhs).as[SignedBV]
    def toInt64: SignedBV = mkInt2BV(64, lhs).as[SignedBV]
    def toReal: RealExpr  = mkInt2Real(lhs)

    def %(rhs: IntExpr): IntExpr = mkMod(lhs, rhs)

    /** Assert lhs is within min and max inclusive. */
    def within(min: IntExpr, max: IntExpr): BoolExpr = lhs >= min && lhs <= max
  }
}
