package pspz3
package context

import com.microsoft.z3._

trait Arrays extends Any with HasContext {
  // ArrayExpr mkArrayConst(String name, Sort domain, Sort range)
  // ArrayExpr mkConstArray(Sort domain, Expr v)
  // ArrayExpr mkEmptySet(Sort domain)
  // ArrayExpr mkFullSet(Sort domain)
  // ArrayExpr mkSetAdd(ArrayExpr set, Expr element)
  // ArrayExpr mkSetComplement(ArrayExpr arg)
  // ArrayExpr mkSetDel(ArrayExpr set, Expr element)
  // ArrayExpr mkSetDifference(ArrayExpr arg1, ArrayExpr arg2)
  // ArrayExpr mkSetIntersection(ArrayExpr... args)
  // ArrayExpr mkSetUnion(ArrayExpr... args)
  // ArrayExpr mkStore(ArrayExpr a, Expr i, Expr v)
  // BoolExpr mkSetMembership(Expr elem, ArrayExpr set)
  // BoolExpr mkSetSubset(ArrayExpr arg1, ArrayExpr arg2)
  // Expr mkSelect(ArrayExpr a, Expr i)
  // Expr mkTermArray(ArrayExpr array)
}
