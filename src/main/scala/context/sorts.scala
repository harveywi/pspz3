package pspz3
package context

import com.microsoft.z3
import z3.BitVecSort
import z3.enumerations.Z3_lbool

trait Sorts extends HasContext {
  self: Bools with Exprs =>

  import ctx._

  def z3boolean(x: Z3_lbool): Opt[Boolean] = x match {
    case Z3_lbool.Z3_L_TRUE  => Some(true)
    case Z3_lbool.Z3_L_FALSE => Some(false)
    case Z3_lbool.Z3_L_UNDEF => None
  }

  lazy val Int32Sort = mkBitVecSort(32)
  lazy val Int64Sort = mkBitVecSort(64)

  implicit def BOOL: ExprSort.Typed[Boolean, BoolExpr, z3.BoolSort] =
    ExprSort(mkBoolConst, mkBool, x => z3boolean(x.getBoolValue))

  implicit def REAL: ExprSort.Typed[BigRational, RealExpr, z3.RealSort] =
    ExprSort(mkRealConst, n => mkReal(n.toString), _.toBigRational)

  implicit def INT: ExprSort.Typed[BigInt, IntExpr, z3.IntSort] =
    ExprSort(mkIntConst, n => mkInt(n.toString), _.toBigInt)

  implicit def INT32: ExprSort.Typed[Int, Int32, Int32Sort.type] =
    ExprSort(mkBVConst(_, 32).as[Int32], mkBV(_, 32).as[Int32], _.toInt32)

  implicit def INT64: ExprSort.Typed[Long, Int64, Int64Sort.type] =
    ExprSort(mkBVConst(_, 64).as[Int64], mkBV(_, 64).as[Int64], _.toInt64)

  // ArraySort mkArraySort(Sort domain, Sort range)
  // BitVecSort mkBitVecSort(int size)
  // BoolSort mkBoolSort()
  // DatatypeSort mkDatatypeSort(String name, Constructor[] constructors)
  // DatatypeSort[] mkDatatypeSorts(String[] names, Constructor[][] c)
  // EnumSort mkEnumSort(String name, String... enumNames)
  // FPRMSort mkFPRoundingModeSort()
  // FPSort mkFPSort(int ebits, int sbits)
  // FPSort mkFPSort128()
  // FPSort mkFPSort16()
  // FPSort mkFPSort32()
  // FPSort mkFPSort64()
  // FPSort mkFPSortDouble()
  // FPSort mkFPSortHalf()
  // FPSort mkFPSortQuadruple()
  // FPSort mkFPSortSingle()
  // FiniteDomainSort mkFiniteDomainSort(String name, long size)
  // IntSort mkIntSort()
  // ListSort mkListSort(String name, Sort elemSort)
  // RealSort mkRealSort()
  // SetSort mkSetSort(Sort ty)
  // TupleSort mkTupleSort(String name, ...)
  // UninterpretedSort mkUninterpretedSort(String str)

  // BoolSort getBoolSort()
  // IntSort getIntSort()
  // RealSort getRealSort()
  // Sort[] getSMTLIBSorts()
  // int getNumSMTLIBSorts()
}
