package pspz3
package context

import com.microsoft.z3._

trait Tactics extends HasContext {
  def numTactics  = ctx.getNumTactics
  def tacticNames = ctx.getTacticNames
  def tacticDescs = tacticNames map (ctx getTacticDescription _)
  def tacticDRQ   = ctx.getTacticDRQ()

  def tactic(name: String): Tactic = ctx mkTactic name
  def parOr(ts: Tactic*): Tactic   = ctx.parOr(ts: _*)

  object tactic {
    def Skip             = ctx.skip()
    def Fail             = ctx.fail()
    def FailIfNotDecided = ctx.failIfNotDecided()
  }

  implicit class TacticOps(t: Tactic) {
    def andThen(t2: Tactic, ts: Tactic*): Tactic = ctx.andThen(t, t2, ts: _*)
    def mkSolver(): Solver                       = ctx.mkSolver(t)
    def orElse(t2: Tactic): Tactic               = ctx.orElse(t, t2)
    def parAndThen(t2: Tactic): Tactic           = ctx.parAndThen(t, t2)
    def repeat(max: Int): Tactic                 = ctx.repeat(t, max)
    def then(t2: Tactic, ts: Tactic*): Tactic    = ctx.then(t, t2, ts: _*)
    def tryFor(ms: Int): Tactic                  = ctx.tryFor(t, ms)
    def usingParams(p: Params): Tactic           = ctx.usingParams(t, p)
    def withParams(p: Params): Tactic            = ctx.`with`(t, p)
  }
}
