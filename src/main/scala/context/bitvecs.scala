package pspz3
package context

import com.microsoft.z3._

trait BitVecs extends Any with HasContext {
  import ctx._

  // mkBVNegNoOverflow
  // mkBVAddNoOverflow
  // mkBVMulNoOverflow
  // mkBVSubNoOverflow
  // mkBVSDivNoOverflow
  // mkBVAddNoUnderflow
  // mkBVMulNoUnderflow
  // mkBVSubNoUnderflow
  // mkBVRedAND
  // mkBVRedOR

  implicit class SignedBitVecExprOps(val lhs: SignedBV) extends BitVecExprOps[SignedBV] {
    private[this] type A     = SignedBV

    def <(rhs: A): BoolExpr  = mkBVSLT(lhs, rhs)
    def <=(rhs: A): BoolExpr = mkBVSLE(lhs, rhs)
    def >(rhs: A): BoolExpr  = mkBVSGT(lhs, rhs)
    def >=(rhs: A): BoolExpr = mkBVSGE(lhs, rhs)
    def /(rhs: A): A         = mkBVSDiv(lhs, rhs)
    def %(rhs: A): A         = mkBVSRem(lhs, rhs)
  }

  implicit class UnsignedBitVecExprOps(val lhs: UnsignedBV) extends BitVecExprOps[UnsignedBV] {
    private[this] type A     = UnsignedBV

    def <(rhs: A): BoolExpr  = mkBVULT(lhs, rhs)
    def <=(rhs: A): BoolExpr = mkBVULE(lhs, rhs)
    def >(rhs: A): BoolExpr  = mkBVUGT(lhs, rhs)
    def >=(rhs: A): BoolExpr = mkBVUGE(lhs, rhs)
    def /(rhs: A): A         = mkBVUDiv(lhs, rhs)
    def %(rhs: A): A         = mkBVURem(lhs, rhs)
  }

  trait BitVecExprOps[A <: BitVecExpr] {
    val lhs: A

    protected implicit def mkA(x: BitVecExpr): A = x.as[A]

    def <(rhs: A): BoolExpr
    def <=(rhs: A): BoolExpr
    def >(rhs: A): BoolExpr
    def >=(rhs: A): BoolExpr
    def /(rhs: A): A
    def %(rhs: A): A

    def +(rhs: A): A = mkBVAdd(lhs, rhs)
    def -(rhs: A): A = mkBVSub(lhs, rhs)
    def *(rhs: A): A = mkBVMul(lhs, rhs)

    def &(rhs: A): A    = mkBVAND(lhs, rhs)
    def |(rhs: A): A    = mkBVOR(lhs, rhs)

    def nand(rhs: A): A = mkBVNAND(lhs, rhs)
    def nor(rhs: A): A  = mkBVNOR(lhs, rhs)
    def xnor(rhs: A): A = mkBVXNOR(lhs, rhs)
    def xor(rhs: A): A  = mkBVXOR(lhs, rhs)

    def toInt: IntExpr         = mkBV2Int(lhs, true)  // signed = true
    def toUnsignedInt: IntExpr = mkBV2Int(lhs, false)
    def unary_- : A            = mkBVNeg(lhs)
    def unary_~ : A            = mkBVNot(lhs)

    def <<(n: A): A  = mkBVSHL(lhs, n)
    def >>(n: A): A  = mkBVLSHR(lhs, n)
    def >>>(n: A): A = mkBVASHR(lhs, n)

    def rotl(n: A): A  = mkBVRotateLeft(lhs, n)
    def rotr(n: A): A  = mkBVRotateRight(lhs, n)
  }
}
