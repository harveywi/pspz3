package pspz3
package context

import com.microsoft.z3

/** TODO: three-value boolean.
 */
trait Bools extends Any with HasContext {
  import ctx._

  def True  = mkTrue()
  def False = mkFalse()

  implicit class BoolExprOps(lhs: BoolExpr) {
    def unary_! : BoolExpr           = mkNot(lhs)
    def &&(rhs: BoolExpr): BoolExpr  = mkAnd(lhs, rhs)
    def ||(rhs: BoolExpr): BoolExpr  = mkOr(lhs, rhs)
    def xor(rhs: BoolExpr): BoolExpr = mkXor(lhs, rhs)

    def ==>(rhs: BoolExpr): BoolExpr = mkImplies(lhs, rhs)
    def <=>(rhs: BoolExpr): BoolExpr = mkIff(lhs, rhs)

    def IFF(rhs: BoolExpr): BoolExpr     = lhs <=> rhs
    def IMPLIES(rhs: BoolExpr): BoolExpr = lhs ==> rhs

    def cond[E <: Expr](e1: E, e2: E): E = mkITE(lhs, e1, e2).as[E]
  }

  // BoolExpr mkDistinct(Expr... args)
  // Quantifier mkExists(...)
  // Quantifier mkForall(...)
  // Quantifier mkQuantifier(...)
}
