package pspz3
package context

import com.microsoft.z3._

trait Probes extends Any with HasContext {
  def numProbes  = ctx.getNumProbes
  def probeNames = ctx.getProbeNames
  def probeDescs = probeNames map (ctx getProbeDescription _)
  def probeDRQ   = ctx.getProbeDRQ()

  def probe(name: String): Probe       = ctx mkProbe name
  def constProbe(value: Double): Probe = ctx constProbe value

  implicit class ProbeOps(p: Probe) {
    def cond(e1: Tactic, e2: Tactic): Tactic = ctx.cond(p, e1, e2)
    def when(t: Tactic): Tactic              = ctx.when(p, t)
    def failIf(): Tactic                     = ctx failIf p

    def <(q: Probe): Probe   = ctx.lt(p, q)
    def >(q: Probe): Probe   = ctx.gt(p, q)
    def <=(q: Probe): Probe  = ctx.le(p, q)
    def >=(q: Probe): Probe  = ctx.ge(p, q)
    def ===(q: Probe): Probe = ctx.eq(p, q)
    def !==(q: Probe): Probe = !(this === q)
    def &&(q: Probe): Probe  = ctx.and(p, q)
    def ||(q: Probe): Probe  = ctx.or(p, q)
    def unary_! : Probe      = ctx.not(p)
  }
}
