package pspz3
package context

import com.microsoft.z3._

trait SmtLib extends HasContext {
  object smtlib {
    import ctx._
    def file(path: jPath): BoolExpr    = parseSMTLIB2File(path.toString, null, null, null, null)
    def code(text: String): BoolExpr   = parseSMTLIB2String(text, null, null, null, null)
    def assumptions(): Array[BoolExpr] = getSMTLIBAssumptions()
    def formulas(): Array[BoolExpr]    = getSMTLIBFormulas()
    def decls(): Array[FuncDecl]       = getSMTLIBDecls()
    def sorts(): Array[Sort]           = getSMTLIBSorts()
  }
}
