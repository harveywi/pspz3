package pspz3
package context

import com.microsoft.z3._

trait Funcs extends Any with HasContext {
  // Expr mkApp(FuncDecl f, Expr... args)
  // Expr mkBound(int index, Sort ty)
  // Expr mkConst(FuncDecl f)
  // Expr mkConst(String name, Sort range)
  // Expr mkFreshConst(String prefix, Sort range)
  // FuncDecl mkConstDecl(String name, Sort range)
  // FuncDecl mkFreshConstDecl(String prefix, Sort range)
  // FuncDecl mkFreshFuncDecl(String prefix, Sort[] domain, Sort range)
  // FuncDecl mkFuncDecl(String name, Sort domain, Sort range)
  // FuncDecl mkFuncDecl(String name, Sort[] domain, Sort range)
  // FuncDecl[] getSMTLIBDecls()
  // Expr MkUpdateField(FuncDecl field, Expr t, Expr v)
  // ArrayExpr mkMap(FuncDecl f, ArrayExpr... args)
}
