package pspz3

import com.microsoft.z3._

trait ToS extends Any {
  def to_s: String
  override def toString = to_s
}

trait Show[-A] {
  def show(x: A): String
  def shown(x: A): Shown = new Shown(show(x))
}

final class Shown(val to_s: String) extends AnyVal with ToS

object Show {
  def native[A](): Show[A]              = Show("" + _)
  def apply[A](f: A => String): Show[A] = new Show[A] { def show(x: A) = f(x) }
}
