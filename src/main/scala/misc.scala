package pspz3

import com.microsoft.z3

trait Signed
trait Unsigned

final case class Assign[+E <: z3.Expr](fn: z3.FuncDecl, value: E) extends ToS {
  def name = fn.getName.toString
  def to_s = s"$name = $value"
}

trait Const[A] {
  def make(name: String): A
}

class NameGen[A](f: Int => A, start: Int) {
  private[this] var current = start
  def apply(): A = try f(current) finally current += 1
}
object NameGen extends NameGen[String]("x" + _, 0)
