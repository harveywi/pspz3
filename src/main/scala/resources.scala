package pspz3

final class EnumerationIterator[A](enum: jEnumeration[A]) extends Iterator[A] {
  def hasNext = enum.hasMoreElements
  def next()  = enum.nextElement()
}

object Resources {
  def getResourceNames(cl: ClassLoader, p: jPath): Array[String] = {
    val path = s"$p/"
    Option(cl getResource path) match {
      case Some(dirUrl) if dirUrl.getProtocol == "file" => new jFile(dirUrl.toURI).list
      case Some(dirUrl) if dirUrl.getProtocol == "jar"  => getResourcesFromJar(path, dirUrl)
      case _                                            =>
        throw new UnsupportedOperationException(s"Cannot get resources for path $p")
    }
  }

  private def getResourcesFromJar(path: String, dirURL: jUrl): Array[String] = {
    val dirPath = dirURL.getPath
    val jarPath = dirPath.substring(5, dirPath indexOf "!")
    val jarFile = new jJar(java.net.URLDecoder.decode(jarPath, "UTF-8"))

    def body(jar: jJar): Array[String] = {
      def resources = new EnumerationIterator(jar.entries).map(_.getName).filter(_ startsWith path).map { name =>
        val entry = name substring path.length
        (entry indexOf "/") |> (i => if (i < 0) entry else entry.substring(0, i))
      }
      resources.toArray.distinct
    }

    try body(jarFile) finally jarFile.close()
  }
}
