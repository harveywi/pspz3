package pspz3
package context

import com.microsoft.z3

/** A bundle of types which relate values, z3 expressions, and z3 sorts.
 *  For instance, a scala Boolean is a z3 BoolExpr with sort BoolSort.
 */
trait ExprSort extends Any {
  type Value
  type Expr <: z3.Expr
  type Sort <: z3.Sort

  def const(name: String): Expr
  def literal[A](value: A)(implicit conv: A => Value): Expr
  def eval(expr: Expr): Opt[Value]
  def toConst: Const[Expr] = new Const[Expr] { def make(name: String) = const(name) }
}

object ExprSort {
  type Typed[V, E <: z3.Expr, S <: z3.Sort] = ExprSort {
    type Value = V
    type Expr  = E
    type Sort  = S
  }

  def apply[V, E <: z3.Expr, S <: z3.Sort](c: String => E, l: V => E, e: E => Opt[V]): Typed[V, E, S] = {
    new ExprSort {
      type Value = V
      type Expr  = E
      type Sort  = S

      def const(name: String): E                         = c(name)
      def literal[A](value: A)(implicit conv: A => V): E = l(conv(value))
      def eval(expr: E): Opt[V]                          = e(expr)
    }
  }
}
