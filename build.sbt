// To pass different args to junit-interface, run it like e.g.
// ARGS="-a -v" ./sbt ~test
def args = sys.env.getOrElse("ARGS", "-a -q -v").split("\\s+").toSeq
def initial = """
import com.microsoft.z3
import pspz3._, context._
val z3c = Z3Context()
import z3c._
"""

                      name := "pspz3"
                   version := "0.1-SNAPSHOT"
              scalaVersion := "2.11.7"
              fork in Test := true
       javaOptions in Test += "-Djava.library.path=/usr/local/lib"
             scalacOptions += "-language:dynamics"
       testOptions in Test += Tests.Argument(TestFrameworks.JUnit, args: _*)
       libraryDependencies += "com.novocode"  % "junit-interface" % "0.11"  % "test"
initialCommands in console := initial
