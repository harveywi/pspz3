## pspz3

Scala bindings for [z3](https://github.com/Z3Prover/z3).

## building

On OSX I had to modify the z3 formula to obtain java support.
```diff
diff --git i/Library/Formula/z3.rb w/Library/Formula/z3.rb
-    system "python", "scripts/mk_make.py", "--prefix=#{prefix}"
+    system "python", "scripts/mk_make.py", "--prefix=#{prefix}", "--java"
```

Getting sbt to find native libraries is a hassle. If you use [my sbt runner](https://github.com/paulp/sbt-extras) it will use the .jvmopts file in this repository, which contains `-Djava.library.path=/usr/local/lib`. Your path may differ. If your runner doesn't support .jvmopts, you may have to pass the option to sbt on the command line.

You'll know if it's working:
```
% sbt console
[...]

scala> sat((x: IntExpr) => x*x === 16 && x < 0)
res0: Option[scala.math.BigInt] = Some(-4)
```
